from typing import Optional

from fastapi import FastAPI

import requests

app = FastAPI()


@app.get("/")
def read_root():
    return "isVegan app"

def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for ingredient in ingredients:
        if 'vegan' in ingredient:
          if ingredient["vegan"]=='no' or ingredient["vegan"]=='maybe':
            return False
    return True


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"isVegan": isVegan(requests.get("https://world.openfoodfacts.org/api/v0/product/{}.json".format(item_id)).json())}

