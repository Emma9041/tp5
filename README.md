# TP5-conception-logicielle

## Pour lancer l'application:

```
git clone https://gitlab.com/Emma9041/tp5.git
cd tp5
pip install -r requirements.txt
uvicorn main:app --reload
```

## Ensuite, pour savoir si un aliment est vegan

Ouvrez l'url http://127.0.0.1:8000/items/ suivi de l'identifiant de l'aliment de votre choix pour afficher s'il est vegan
