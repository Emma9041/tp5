import pytest
from main import isVegan
import json

def test_2_egal_2():
  assert 2==2

def load_params_from_json(json_path):
    with open(json_path) as f:
        return json.load(f)


def test_rozana_isVegan_true():
    rozana_data=load_params_from_json('rozana.json')
    assert isVegan(rozana_data) == True

def test_brioche_isVegan_false():
    brioche_data=load_params_from_json('brioche.json')
    assert isVegan(brioche_data) == False
